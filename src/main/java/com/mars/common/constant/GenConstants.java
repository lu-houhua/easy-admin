package com.mars.common.constant;

/**
 * 代码生成通用常量
 *
 * @author mars
 */
public class GenConstants {
    /**
     * 单表（增删改查）
     */
    public static final String TPL_CRUD = "crud";

    /**
     * 树表（增删改查）
     */
    public static final String TPL_TREE = "tree";

    /**
     * 树编码字段
     */
    public static final String TREE_CODE = "treeCode";

    /**
     * 主子表（增删改查）
     */
    public static final String TPL_SUB = "sub";

    /**
     * 树父编码字段
     */
    public static final String TREE_PARENT_CODE = "treeParentCode";

    /**
     * 树名称字段
     */
    public static final String TREE_NAME = "treeName";

    /**
     * 数据库字符串类型
     */
    public static final String[] COLUMNTYPE_STR = {"char" , "varchar" , "narchar" , "varchar2" , "tinytext" , "text" ,
            "mediumtext" , "longtext"};

    /**
     * 数据库时间类型
     */
    public static final String[] COLUMNTYPE_TIME = {"datetime" , "time" , "date" , "timestamp"};

    /**
     * 数据库数字类型
     */
    public static final String[] COLUMNTYPE_NUMBER = {"tinyint" , "smallint" , "mediumint" , "int" , "number" , "integer" ,
            "bigint" , "float" , "float" , "double" , "decimal" , "bigint unsigned" , "bigint" , "bit" , "tinyint unsigned"};

    /**
     * 页面不需要编辑字段
     */
    public static final String[] COLUMNNAME_NOT_EDIT = {"id" , "create_by" , "create_time" , "del_flag" , "create_by_name" , "update_by_id" , "create_by_id" , "update_by_name" , "deleted"};

    /**
     * 页面不需要显示的列表字段
     */
    public static final String[] COLUMNNAME_NOT_LIST = {"id" , "create_by" , "del_flag" , "update_by" ,
            "update_time" , "create_by_name" , "update_by_id" , "create_by_id" , "update_by_name" , "deleted"};

    /**
     * 页面不需要查询字段
     */
    public static final String[] COLUMNNAME_NOT_QUERY = {"id" , "create_by" , "create_time" , "del_flag" , "update_by" ,
            "update_time" , "remark" , "create_by_name" , "update_by_id" , "create_by_id" , "update_by_name" , "deleted"};

    /**
     * Entity基类字段
     */
    public static final String[] BASE_ENTITY = {"create_time" , "update_time" , "create_by_name" , "update_by_id" , "create_by_id" , "update_by_name" , "deleted"};

    /**
     * Tree基类字段
     */
    public static final String[] TREE_ENTITY = {"parentName" , "parentId" , "orderNum" , "ancestors" , "children"};

    /**
     * 文本框
     */
    public static final String HTML_INPUT = "input";

    /**
     * 文本域
     */
    public static final String HTML_TEXTAREA = "textarea";

    /**
     * 下拉框
     */
    public static final String HTML_SELECT = "select";

    /**
     * 单选框
     */
    public static final String HTML_RADIO = "radio";

    /**
     * 复选框
     */
    public static final String HTML_CHECKBOX = "checkbox";

    /**
     * 日期控件
     */
    public static final String HTML_DATETIME = "datetime";

    /**
     * 字符串类型
     */
    public static final String TYPE_STRING = "String";

    /**
     * 整型
     */
    public static final String TYPE_INTEGER = "Integer";

    /**
     * 长整型
     */
    public static final String TYPE_LONG = "Long";

    /**
     * 浮点型
     */
    public static final String TYPE_DOUBLE = "Double";

    /**
     * 高精度计算类型
     */
    public static final String TYPE_BIGDECIMAL = "BigDecimal";

    /**
     * 时间类型
     */
    public static final String TYPE_DATE = "LocalDateTime";
    /**
     * 时间类型
     */
    public static final String TYPE_LOCAL_DATE_TIME = "LocalDateTime";

    /**
     * 模糊查询
     */
    public static final String QUERY_LIKE = "LIKE";

    /**
     * 需要
     */
    public static final String REQUIRE = "1";
    /**
     * 插入字段
     */
    public static final String[] COLUMNNAME_NOT_INSERT = {"create_by" , "create_time" , "del_flag" , "update_by" ,
            "update_time" , "remark" , "create_by_name" , "update_by_id" , "create_by_id" , "update_by_name" , "deleted"};
    ;
}
