package com.mars.module.oss.utils;

import com.baomidou.mybatisplus.core.toolkit.CollectionUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.mars.framework.async.AsyncFactory;
import com.mars.framework.exception.ServiceException;
import com.mars.module.admin.entity.SysOss;
import com.mars.module.admin.mapper.SysOssMapper;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 常量类，读取配置文件application.properties中的配置
 *
 * @author 程序员Mars
 */
@Component
public class ConstantPropertiesUtil implements InitializingBean {

    @Resource
    private SysOssMapper sysOssMapper;

    @Value("${easy.admin.ossEnable}")
    private boolean ossEnable;

    public static String END_POINT;
    public static String ACCESS_KEY_ID;
    public static String ACCESS_KEY_SECRET;
    public static String BUCKET_NAME;

    @Override
    public void afterPropertiesSet() {
        if (ossEnable) {
            AsyncFactory.runAsync(() -> {
                // 查询数据库oss配置赋值
                List<SysOss> sysOssList = sysOssMapper.selectList(null);
                if (CollectionUtils.isEmpty(sysOssList)) {
                    throw new ServiceException("当前系统未配置oss");
                }
                SysOss sysOss = sysOssList.get(0);
                END_POINT = sysOss.getEndpoint();
                ACCESS_KEY_ID = sysOss.getAccessKey();
                ACCESS_KEY_SECRET = sysOss.getSecretKey();
                BUCKET_NAME = sysOss.getBucketName();
            });
        }
    }
}
