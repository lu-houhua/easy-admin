package com.mars.module.admin.controller;


import java.util.Arrays;
import com.mars.common.enums.BusinessType;
import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.module.admin.entity.SysOss;
import io.swagger.annotations.Api;
import com.mars.framework.annotation.Log;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.admin.service.ISysOssService;
import com.mars.module.admin.request.SysOssRequest;

/**
 * 文件存储控制层
 *
 * @author mars
 * @date 2023-11-20
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "文件存储接口管理",tags = "文件存储接口管理")
@RequestMapping("/admin/sysOss" )
public class SysOssController {

    private final ISysOssService iSysOssService;

    /**
     * 分页查询文件存储列表
     */
    @ApiOperation(value = "分页查询文件存储列表")
    @PostMapping("/pageList")
    public R<PageInfo<SysOss>> list(@RequestBody SysOssRequest sysOss) {
        return R.success(iSysOssService.pageList(sysOss));
    }

    /**
     * 获取文件存储详细信息
     */
    @ApiOperation(value = "获取文件存储详细信息")
    @GetMapping(value = "/query/{id}" )
    public R<SysOss> getInfo(@PathVariable("id" ) Long id) {
        return R.success(iSysOssService.getById(id));
    }

    /**
     * 新增文件存储
     */
    @Log(title = "新增文件存储", businessType = BusinessType.INSERT)
    @ApiOperation(value = "新增文件存储")
    @PostMapping("/add")
    public R<Void> add(@RequestBody SysOssRequest sysOss) {
        iSysOssService.add(sysOss);
        return R.success();
    }

    /**
     * 修改文件存储
     */
    @Log(title = "修改文件存储", businessType = BusinessType.UPDATE)
    @ApiOperation(value = "修改文件存储")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody SysOssRequest sysOss) {
        iSysOssService.update(sysOss);
        return R.success();
    }

    /**
     * 删除文件存储
     */
    @Log(title = "删除文件存储", businessType = BusinessType.DELETE)
    @ApiOperation(value = "删除文件存储")
    @PostMapping("/delete/{ids}" )
    public R<Void> remove(@PathVariable Long[] ids) {
        iSysOssService.deleteBatch(Arrays.asList(ids));
        return R.success();
    }
}
