package com.mars.module.tool.controller;


import java.util.Arrays;

import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.module.tool.entity.SysLoginRecord;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import lombok.extern.slf4j.Slf4j;
import com.mars.module.tool.service.ISysLoginRecordService;
import com.mars.module.tool.request.SysLoginRecordRequest;

/**
 * 系统访问记录控制层
 *
 * @author mars
 * @date 2023-11-17
 */
@Slf4j
@AllArgsConstructor
@RestController
@Api(value = "系统访问记录接口管理",tags = "系统访问记录接口管理")
@RequestMapping("/admin/sysLoginRecord" )
public class SysLoginRecordController {

    private final ISysLoginRecordService iSysLoginRecordService;

    /**
     * 分页查询系统访问记录列表
     */
    @ApiOperation(value = "分页查询系统访问记录列表")
    @PostMapping("/pageList")
    public R<PageInfo<SysLoginRecord>> list(@RequestBody SysLoginRecordRequest sysLoginRecord) {
        return R.success(iSysLoginRecordService.pageList(sysLoginRecord));
    }

    /**
     * 获取系统访问记录详细信息
     */
    @ApiOperation(value = "获取系统访问记录详细信息")
    @GetMapping(value = "/query/{infoId}" )
    public R<SysLoginRecord> getInfo(@PathVariable("infoId" ) Long infoId) {
        return R.success(iSysLoginRecordService.getById(infoId));
    }

    /**
     * 新增系统访问记录
     */
    @ApiOperation(value = "新增系统访问记录")
    @PostMapping("/add")
    public R<Void> add(@RequestBody SysLoginRecordRequest sysLoginRecord) {
        iSysLoginRecordService.add(sysLoginRecord);
        return R.success();
    }

    /**
     * 修改系统访问记录
     */
    @ApiOperation(value = "修改系统访问记录")
    @PostMapping("/update")
    public R<Void> edit(@RequestBody SysLoginRecordRequest sysLoginRecord) {
        iSysLoginRecordService.update(sysLoginRecord);
        return R.success();
    }

    /**
     * 删除系统访问记录
     */
    @ApiOperation(value = "删除系统访问记录")
    @PostMapping("/delete/{infoIds}" )
    public R<Void> remove(@PathVariable Long[] infoIds) {
        iSysLoginRecordService.deleteBatch(Arrays.asList(infoIds));
        return R.success();
    }
}
