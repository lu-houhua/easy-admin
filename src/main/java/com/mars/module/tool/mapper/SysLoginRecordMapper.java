package com.mars.module.tool.mapper;

import com.mars.module.tool.entity.SysLoginRecord;
import com.mars.framework.mapper.BasePlusMapper;

/**
 * 系统访问记录Mapper接口
 *
 * @author mars
 * @date 2023-11-17
 */
public interface SysLoginRecordMapper extends BasePlusMapper<SysLoginRecord> {

}
