package com.mars.module.tool.request;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 菜单权限表 sys_menu
 *
 * @author mars
 */
@Data
public class GenSysMenuRequest {

    /**
     * 菜单ID
     */
    private Long id;

    /**
     * 1.目录 2 菜单 3 按钮
     */
    private Integer menuType;

    /**
     * 名称
     */
    private String menuName;

    /**
     * URL
     */
    private String url;

    /**
     * 父ID
     */
    private Long parentId;

    /**
     * 排序
     */
    private Integer sort;

    /**
     * 图标
     */
    private String icon;


    /**
     * 子菜单
     */
    private List<GenSysMenuRequest> children = new ArrayList<>();

}
