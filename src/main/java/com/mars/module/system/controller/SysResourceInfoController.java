package com.mars.module.system.controller;

import com.mars.common.request.sys.*;
import com.mars.common.response.PageInfo;
import com.mars.common.result.R;
import com.mars.module.system.entity.ResourceInfo;
import com.mars.module.system.service.IResourceInfoService;
import com.mars.module.system.service.ISysMenuService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.AllArgsConstructor;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * 资源信息管理控制器
 *
 * @author 源码字节-程序员Mars
 */
@RestController
@RequestMapping("/sys/resourceInfo")
@Api(tags = "系统管理-资源信息管理控制器")
@AllArgsConstructor
public class SysResourceInfoController {

    private final IResourceInfoService resourceInfoService;

    /**
     * 资源列表
     *
     * @param request 请求参数
     * @return R
     */
    @PostMapping("/pageList")
    @ApiOperation(value = "资源列表")
    public R<PageInfo<ResourceInfo>> list(@Validated @RequestBody ResourceInfoRequest request) {
        return R.success(resourceInfoService.pageList(request));
    }


    /**
     * 数据采集
     *
     * @param request 请求参数
     * @return R
     */
    @PostMapping("/fetchData")
    @ApiOperation(value = "数据采集")
    public R<Void> fetchData(@RequestBody ResourceInfoFetchRequest request) throws IOException {
        resourceInfoService.fetchData(request.getType(), request.getPage());
        return R.success();
    }


}
