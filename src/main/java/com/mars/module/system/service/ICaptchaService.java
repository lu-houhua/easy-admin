package com.mars.module.system.service;

import com.mars.common.response.sys.CaptchaResponse;

/**
 * @author mars
 * @version 1.0
 * @date 2023/10/31 22:26
 */
public interface ICaptchaService {

    /**
     * 获取验证码
     */
    CaptchaResponse acquire();


}
