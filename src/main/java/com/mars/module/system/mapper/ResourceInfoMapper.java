package com.mars.module.system.mapper;

import com.mars.framework.mapper.BasePlusMapper;
import com.mars.module.system.entity.ResourceInfo;

public interface ResourceInfoMapper extends BasePlusMapper<ResourceInfo> {


}
