let prefix = '';
new Vue({
    el: '#app',
    data() {
        return {
            activePath: '',
            dataDialog: false,
            pwdDialog: false,
            editableTabsValue: '',
            systemName: '',
            searchText: '',
            currentTime: '',
            editableTabs: [],
            sysUser: {},
            sysMenu: [],
            avatar: '',
            uploadUrl: prefix + '/common/file/upload',
            uploadHeaders: '',
            uploadData: {'module': '1'},
            dataForm: {
                id: '',
                sex: '',
                birthDate: '',
                phone: '',
                province: '',
                city: '',
                area: '',
                address: '',
                avatar: ''
            },
            dataFormRules: {
                sex: [
                    {required: true, message: '请选择', trigger: 'blur'}
                ],
                birthDate: [
                    {required: true, message: '请选择', trigger: 'blur'}
                ],
                phone: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 11, max: 11, message: '手机号码格式不正确', trigger: 'blur'}
                ]
            },
            pwdForm: {
                oldPwd: '',
                newPwd: '',
                confirmPwd: ''
            },
            pwdFormRules: {
                oldPwd: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 2, max: 10, message: '长度在 2 到 10 个字符', trigger: 'blur'}
                ],
                newPwd: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 2, max: 10, message: '长度在 2 到 10 个字符', trigger: 'blur'}
                ],
                confirmPwd: [
                    {required: true, message: '请输入', trigger: 'blur'},
                    {min: 2, max: 10, message: '长度在 2 到 10 个字符', trigger: 'blur'}
                ]
            },
            regionList: []
        }
    },
    computed: {

        formattedTime: function () {
            var date = new Date();
            var year = date.getFullYear();
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var day = ("0" + date.getDate()).slice(-2);
            var hours = ("0" + date.getHours()).slice(-2);
            var minutes = ("0" + date.getMinutes()).slice(-2);
            var seconds = ("0" + date.getSeconds()).slice(-2);
            return year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
        }
    },
    created() {
        let sysUser = localStorage.getItem("mars-sysUser")
        this.updateTime(); // 初始加载时间
        setInterval(this.updateTime, 1000);
        if (sysUser) {
            this.sysUser = JSON.parse(sysUser);
            this.sysMenu = JSON.parse(localStorage.getItem("mars-sysMenu"));
            this.avatar = prefix + this.sysUser.avatar;
            this.uploadHeaders = {"token": JSON.parse(localStorage.getItem("mars-token"))};
            this.addTab('首页', '/home/home.html');
        }
        this.getConfigInfo()

    },
    mounted: {},
    watch: {
        searchText(newText) {
            if (newText) {
                let menus = JSON.parse(localStorage.getItem("mars-sysMenu"));
                this.sysMenu = menus.filter(function (item) {
                    return item.menuName.toLowerCase().includes(newText.toLowerCase());
                });
            } else {
                this.sysMenu = JSON.parse(localStorage.getItem("mars-sysMenu"));
            }

        }
    },
    methods: {

        updateTime() {
            var date = new Date();
            var year = date.getFullYear();
            var month = ("0" + (date.getMonth() + 1)).slice(-2);
            var day = ("0" + date.getDate()).slice(-2);
            var hours = ("0" + date.getHours()).slice(-2);
            var minutes = ("0" + date.getMinutes()).slice(-2);
            var seconds = ("0" + date.getSeconds()).slice(-2);
            this.currentTime = year + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + seconds;
        },
        getConfigInfo() {
            requests.get("/admin/sysConfig/sysInfo").then(res => {
                this.systemName = res.data.systemName
            })

        },
        handleCommand(cmd) {
            if (cmd === 'updateData') {
                this.dataDialog = true;
                let sysUser = JSON.parse(localStorage.getItem("mars-sysUser"));
                this.dataForm = sysUser;
            } else if (cmd === 'updatePwd') {
                this.pwdDialog = true;
                if (this.$refs.pwdForm !== undefined) {
                    this.$refs.pwdForm.resetFields();
                }
            } else if (cmd === 'logout') {
                requests.post('/logout').then((res) => {
                    if (res.code === '200' || res.code === '9999') {
                        localStorage.clear();
                        parent.location.href = '/login';
                    }
                });
            }
        },
        addTab(tabName, tabUrl) {
            let isExist = false;
            this.editableTabs.forEach((tab, index) => {
                if (tab.title === tabName) {
                    isExist = true;
                    return;
                }
            });
            if (isExist) {
                this.editableTabsValue = tabUrl;
                return;
            }
            this.editableTabs.push({
                title: tabName,
                name: tabUrl,
                content: prefix + tabUrl
            });
            this.editableTabsValue = tabUrl;
            location.hash = '' + this.editableTabsValue;
        },
        clickTab(tab, event) {
            location.hash = '' + tab.name;
            this.activePath = tab.name;
        },
        removeTab(tabName) {
            let tabs = this.editableTabs;
            let activeName = this.editableTabsValue;
            if (activeName === tabName) {
                tabs.forEach((tab, index) => {
                    if (tab.name === tabName) {
                        let nextTab = tabs[index + 1] || tabs[index - 1];
                        if (nextTab) {
                            activeName = nextTab.name;
                        }
                    }
                });
            }
            this.editableTabsValue = activeName;
            this.editableTabs = tabs.filter(tab => tab.name !== tabName);
            location.hash = '' + this.editableTabsValue;
            this.activePath = this.editableTabsValue;
        },
        handleAvatarSuccess(idx, res, file, name) {
            if (res.code === '200') {
                this.dataForm.avatar = prefix + '/common/file/download?id=' + res.data.url;
            }
        },
        beforeAvatarUpload(file) {
            let isLt2M = file.size / 1024 / 1024 < 2;
            if (!isLt2M) {
                this.$message.error('上传头像图片大小不能超过2MB!');
            }
            return isLt2M;
        },
        dataFormSubmit() {
            this.$refs.dataForm.validate((valid) => {
                if (valid) {
                    this.dataDialog = false;
                    let param = this.dataForm;
                    requests.post('/updateInfo', JSON.parse(JSON.stringify(param)))
                        .then((res) => {
                            if (res.code === '200') {
                                let sysUser = JSON.parse(localStorage.getItem("mars-sysUser"));
                                sysUser.id = param.id;
                                sysUser.sex = param.sex;
                                sysUser.birthDate = param.birthDate;
                                sysUser.phone = param.phone;
                                sysUser.province = param.province;
                                sysUser.city = param.city;
                                sysUser.area = param.area;
                                sysUser.address = param.address;
                                sysUser.avatar = param.avatar;
                                localStorage.setItem('mars-sysUser', JSON.stringify(sysUser));
                                this.$message({
                                    type: 'success',
                                    message: '修改成功'
                                });
                            } else {
                                this.$message.error(res.message);
                            }
                        });
                } else {
                    return false;
                }
            });
        },
        pwdFormSubmit() {
            this.$refs.pwdForm.validate((valid) => {
                if (valid) {
                    let param = this.pwdForm;
                    requests.post('/pwd/update', JSON.parse(JSON.stringify(param)))
                        .then((res) => {
                            if (res.code === '200') {
                                parent.layer.msg('修改成功');
                                this.pwdDialog = false;
                            } else {
                                parent.layer.msg(res.message);
                            }
                        });
                } else {
                    return false;
                }
            });
        }
    },

});
