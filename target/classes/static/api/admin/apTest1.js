/**
 * 分页查询测试1列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/apTest1/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询测试1详细
 * @param id
 * @returns {*}
 */
function detail(id) {
    return requests({
        url: '/admin/apTest1/query/' + id,
        method: 'get'
    })
}

/**
 * 新增测试1
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/apTest1/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改测试1
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/apTest1/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除测试1
 * @param id
 * @returns {*}
 */
function del(id) {
    return requests({
        url: '/admin/apTest1/delete/' + id,
        method: 'delete'
    })
}

/**
 * 导出测试1
 *
 * @param query
 * @returns {*}
 */
function exportTest1(query) {
    return requests({
        url: '/admin/apTest1/export',
        method: 'get',
        params: query
    })
}
