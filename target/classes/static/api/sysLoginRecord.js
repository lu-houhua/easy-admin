/**
 * 分页查询系统访问记录列表
 *
 * @param data
 * @returns {*}
 */
function pageList(data) {
    return requests({
        url: '/admin/sysLoginRecord/pageList',
        method: 'post',
        data: data
    })
}

/**
 * 查询系统访问记录详细
 * @param id
 * @returns {*}
 */
function detail(infoId) {
    return requests({
        url: '/admin/sysLoginRecord/query/' + infoId,
        method: 'get'
    })
}

/**
 * 新增系统访问记录
 *
 * @param data
 * @returns {*}
 */
function add(data) {
    return requests({
        url: '/admin/sysLoginRecord/add',
        method: 'post',
        data: data
    })
}

/**
 * 修改系统访问记录
 *
 * @param data
 * @returns {*}
 */
function update(data) {
    return requests({
        url: '/admin/sysLoginRecord/update',
        method: 'post',
        data: data
    })
}

/**
 *  删除系统访问记录
 * @param id
 * @returns {*}
 */
function del(infoId) {
    return requests({
        url: '/admin/sysLoginRecord/delete/' + infoId,
        method: 'delete'
    })
}

/**
 * 导出系统访问记录
 *
 * @param query
 * @returns {*}
 */
function exportLoginRecord(query) {
    return requests({
        url: '/admin/sysLoginRecord/export',
        method: 'get',
        params: query
    })
}
